# Comentarios Varios

## Implementación de ArrayList

Para los que preguntaban como estaba implementada la clase `ArrayList`
en Java:
<http://hg.openjdk.java.net/jdk7/jdk7/jdk/file/tip/src/share/classes/java/util/ArrayList.java>.
Efectivamente, es un array que se va redimensionando a medida que se van
agregando elementos.
